#include "ArduinoReset.h"

#if defined(ARDUINO_ARCH_ESP8266)

#include <Esp.h>
#include <user_interface.h>

#elif defined(ARDUINO_ARCH_SAMD)

#include <Reset.h>

#endif

static const char * const resetCauseStrings[] PROGMEM =
  { "unknown", "power on", "external", "brownout", "software watchdog"
  , "hardware watchdog", "jtag", "USB", "software", "UPDI", "deep sleep"
  , "exception"
  };

const char *Reset::resetCauseString(enum Reset::resetCause r)
{
  return resetCauseStrings[r];
}

void Reset::softwareReset() {
#if defined(ARDUINO_ARCH_AVR) || defined(ARDUINO_ARCH_MEGAAVR)
  asm volatile (" jmp 0");
#elif defined(ARDUINO_ARCH_ESP8266)
  ESP.restart();
#elif defined(ARDUINO_ARCH_SAMD)
  initiateReset(0);
#else
# error "Reset::softwareReset() -- unsupported architecture"
#endif
}

enum Reset::resetCause Reset::resetCause() {
  enum resetCause r = unknown;
#if defined(ARDUINO_ARCH_AVR)
  int reg = MCUSR;
  if (reg == 0 || bitRead(reg, 0) > 0) r = poweron;
  else if (bitRead(reg, 1) > 0)        r = external;
  else if (bitRead(reg, 2) > 0)        r = brownout;
  else if (bitRead(reg, 3) > 0)        r = swwatchdog;
  else if (bitRead(reg, 4) > 0)        r = jtag;
  else if (bitRead(reg, 5) > 0)        r = usb;
  MCUSR = 0x0;
#elif defined(ARDUINO_ARCH_MEGAAVR)
  int reg = MCUSR;
  if (reg == 0 || bitRead(reg, 0) > 0) r = poweron;
  else if (bitRead(reg, 1) > 0)        r = brownout;
  else if (bitRead(reg, 2) > 0)        r = external;
  else if (bitRead(reg, 3) > 0)        r = swwatchdog;
  else if (bitRead(reg, 4) > 0)        r = software;
  else if (bitRead(reg, 5) > 0)        r = updi;
  MCUSR = 0x0
#elif defined(ARDUINO_ARCH_ESP8266)
  struct rst_info *rc = ESP.getResetInfoPtr();
  switch (rc->reason) {
    case REASON_DEFAULT_RST:      r = poweron;    break;
    case REASON_WDT_RST:          r = hwwatchdog; break;
    case REASON_EXCEPTION_RST:    r = exception;  break;
    case REASON_SOFT_WDT_RST:     r = swwatchdog; break;
    case REASON_SOFT_RESTART:     r = software;   break;
    case REASON_DEEP_SLEEP_AWAKE: r = deepsleep;  break;
    case REASON_EXT_SYS_RST:      r = external;   break;
    default:                      r = unknown;    break;
  }
#elif defined(ARDUINO_ARCH_SAMD)
  if (REG_PM_RCAUSE == PM_RCAUSE_SYST)       r = software;
  else if (REG_PM_RCAUSE == PM_RCAUSE_WDT)   r = swwatchdog;
  else if (REG_PM_RCAUSE == PM_RCAUSE_EXT)   r = external;
  else if (REG_PM_RCAUSE == PM_RCAUSE_BOD33) r = brownout;
  else if (REG_PM_RCAUSE == PM_RCAUSE_BOD12) r = brownout;
  else if (REG_PM_RCAUSE == PM_RCAUSE_POR)   r = poweron;
#else
# error "Reset::resetCause() -- unsupported architecture"
#endif
  return r;
}
