#ifndef ARDUINORESET_H
#define ARDUINORESET_H

#include <Arduino.h>

class Reset
{
  public:
    /**
     * All causes of resets
     */
    enum resetCause { unknown, poweron, external, brownout, swwatchdog, hwwatchdog, jtag, usb, software, updi, deepsleep, exception };
    /**
     * String representations of the reset causes
     */
    static const char *resetCauseString(enum resetCause);

    /**
     * Reset the microcontroller from software
     */
    static void softwareReset();

    /**
     * Retrieve the reset cause. Call this function first in setup().
     */
    static enum resetCause resetCause();
};

extern Reset Reset;

#endif /* !RESET_H */
