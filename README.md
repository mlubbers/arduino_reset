# Arduino library: Reset

This is a helper library that provides a device agnostic interface for software
resetting and retrieving the cause of a (re)boot.

## Supported architectures:

- AVR (by reading the MCUSR register)
- MegaAVR (by reading the MCUSR register)
- ESP8266 (by using ESP.getResetInfo() which uses system_get_rst_info())

## License

MIT License, see [LICENSE](LICENSE)
