#include <Reset.h>

/*
  Boots up the devices and prints the resetcause to the serial monitor. Note
  that the reset cause is fetched before anything else (such as starting the
  Serial port).

  After five seconds, the device is reset.
*/

void setup() {
  enum Reset::resetCause r = Reset.resetCause();
  Serial.begin(9600);
  while(!Serial)
    delay(1);
  Serial.print("Boot up cause: ");
  Serial.println(Reset.resetCauseString(r));
  delay(5000);
  Reset.softwareReset();
}

void loop() {
  delay(10);
}
